// alert('hi')

// Object
	// Syntax
		/*
			let objectName = {
				keyA : value A,
				keyB : value B
			}
			keyA : valueA === key/ property : value pairs
		*/

	let cellphone = {
		name: 'Nokia 3210',
		manufactureDate: 1999
	};

	console.log(cellphone);
	console.log(typeof cellphone);

// Creating objects using constructor function
	// Syntax:
		/*
			function objectName(keyA, keyB){
				this.keyA = keyA;
				this.keyB = keyB;
			}

		*/

		function Laptop(name, manufactureDate){
				this.name = name;
				this.manufactureDate = manufactureDate;

				// return name + manufactureDate
		};

		// instantiate- unique instance or creating an instance (object)

		let laptop = new Laptop('Lenovo', 2008);
		console.log(laptop);

		let oldLaptop = new Laptop('Portal R2E CCMC', 1980);
		console.log(oldLaptop);

// Creating empty objects
	let computer = {};
	let myComputer = new Object();

	console.log(myComputer);

// Accessing array objects

let array = [laptop, oldLaptop];

console.log(array[0]["name"]);

// dot notation
console.log(array[0].name);

// Initializing/ Adding/ Deleting/ Reassigning Object Properties

let car = {};

// adding using dot notation

car.name = 'Honda Civic';
console.log(car);

// adding using bracket notation
car['manufactureDate'] = 2019;
console.log(car);

car.color = 'red'

// Deleting property objects
delete car["manufactureDate"];
console.log(car);

delete car.color;
console.log(car);

// Reassigning object
car.name = 'Dodge Charger R/T';
console.log(car);

car['name'] = 'Toyota Wigo';
console.log(car);

// Object Methods

let friend = {
	firstName: 'Ochako',
	lastName: 'Uraraka',
	address: {
		city: 'Osaka',
		country: 'Japan'
	},
	emails: ['ouraraka@mail.com', 'ochako@mail.com'],
	introduce: function(){
		console.log('Hello my name is ' + this.firstName);
		console.log('My preferred email is ' + this.emails[0]);
	}
}

friend.introduce();

function Pokemon(name, lvl, hp){
	this.name = name;
	this.level = lvl;
	this.health = hp * 2;
	this.attack = lvl;

	// methods
	this.tackle = function(target){
		console.log(this.name + ' '+ 'tackled ' + target.name)
	};

	this.faint = function(){
		console.log(this.name + ' ' + 'fainted')
	};
}

// creating instance
let bulbasaur = new Pokemon('Bulbasaur', 3, 30);
let charizard = new Pokemon('Charizard', 2, 20); 

bulbasaur.tackle(charizard);
charizard.tackle(bulbasaur);
bulbasaur.faint();
